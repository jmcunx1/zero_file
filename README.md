## zerof - overwrite a file with selected characters

This is a little utility created a while ago for systems
without dd(1).  All it does is overwrite an existing file
with a character of your choice (or random data).

This should **not** be used on Solid-state drives
or similar devices.  If used, I doubt it will really clear
the empty space.  I am *not* sure if this will overwrite
data on some Modern File Systems.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/zero_file) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/zero\_file.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/zero\_file.gmi (mirror)

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
